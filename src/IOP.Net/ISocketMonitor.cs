﻿using System;
using System.Threading.Tasks;

namespace IOP.Net
{
    /// <summary>
    /// 嵌套字监听者接口
    /// </summary>
    public interface ISocketMonitor : IDisposable
    {
        /// <summary>
        /// 标识符
        /// </summary>
        string Identifier { get; }
        /// <summary>
        /// 唯一标识符
        /// </summary>
        Guid Guid { get; }
        /// <summary>
        /// 无报文最长生存时间
        /// </summary>
        int KeepAlive { get; }
        /// <summary>
        /// 是否启用保持连接
        /// </summary>
        bool EnableKeepAlive { get; }
        /// <summary>
        /// 超时事件
        /// </summary>
        event Action<ISocketMonitor> KeepAliveTimeout;
        /// <summary>
        /// 资源是否已被释放
        /// </summary>
        bool IsDispose { get; }
        /// <summary>
        /// 是否连接
        /// </summary>
        bool IsConnected { get; }
        /// <summary>
        /// 更新上次报文上送时间
        /// </summary>
        /// <param name="time"></param>
        void UpdateLastMessageTime(DateTime time);
        /// <summary>
        /// 允许保持连接并启动检查函数
        /// </summary>
        /// <param name="keepAlive"></param>
        void EnableKeepAliveCheckAndStart(int keepAlive = 120);
        /// <summary>
        /// 取消保持连接检查函数
        /// </summary>
        void DisableKeepAliveCheck();
        /// <summary>
        /// 重置
        /// </summary>
        void Restart();
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task SendAsync(ReadOnlyMemory<byte> data);
        /// <summary>
        /// 关闭连接
        /// </summary>
        /// <returns></returns>
        Task CloseAsync();
    }
}
