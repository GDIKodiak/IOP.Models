﻿using IOP.Models.Message;
using IOP.Models.Query;
using IOP.Models.Tree.BinaryTree;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class RedBlackTreeTest
    {
        #region 插入测试
        [TestMethod]
        public void PutCase1Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(12, 12);
                test.Put(8, 8);
                test.Put(20, 20);
                test.Put(4, 4);
                test.Put(9, 9);
                test.Put(16, 16);
                test.Put(2, 2);
                Assert.IsTrue(test.Root.Left.Color);
                Assert.IsFalse(test.Root.Right.Color);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PutCase2Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(12, 12);
                test.Put(8, 8);
                test.Put(9, 9);
                test.Put(4, 4);
                test.Put(6, 6);
                test.Put(2, 2);
                test.Put(1, 1);
                Assert.AreEqual(test.Root.Value, 9);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PutCase3Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(9, 9);
                test.Put(6, 6);
                test.Put(4, 4);
                Assert.AreEqual(6, test.Root.Value);
                test.Put(3, 3);
                test.Put(2, 2);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PutCase4Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(9, 9);
                test.Put(4, 4);
                test.Put(12, 12);
                test.Put(18, 18);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PutCase5Test()
        {
            try
            
{
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(9, 9);
                test.Put(12, 12);
                test.Put(10, 10);
                Assert.AreEqual(10, test.Root.Value);
                test.Put(16, 16);
                test.Put(13, 13);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PutCase6Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(9, 9);
                test.Put(10, 10);
                test.Put(12, 12);
                Assert.AreEqual(10, test.Root.Value);
                test.Put(13, 13);
                test.Put(16, 16);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        #endregion

        #region 删除测试
        [TestMethod]
        public void DeleteRootAndNoChildrenTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Delete(20);
                Assert.IsNull(test.Root);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteWhenChildrenAreAllNullTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Delete(50);
                test.Delete(88);
                test.Delete(30);
                Assert.IsFalse(test.Contains(50, out int result));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteWhenLeftChildrenAreNullTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10,10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(50, 50);
                test.Delete(40);
                Assert.IsFalse(test.Contains(40, out int result));
                Assert.AreEqual(test.Root.Right.Value, 50);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteWhenRightChildrenAreNullTest()
        {
            try
            
{
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Delete(40);
                Assert.IsFalse(test.Contains(40, out int result));
                Assert.AreEqual(test.Root.Right.Value, 30);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteRootTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Delete(20);
                Assert.IsFalse(test.Contains(20, out int result));
                Assert.AreEqual(test.Root.Value, 10);
                test.Put(5, 5);
                test.Put(40, 40);
                test.Delete(10);
                Assert.IsFalse(test.Contains(10, out int result2));
                Assert.AreEqual(test.Root.Value, 40);
                test.Delete(40);
                test.Delete(5);
                Assert.IsNull(test.Root);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase1Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(25, 25);
                test.Delete(10);
                test.Delete(15);
                test.Delete(5);
                Assert.IsFalse(test.Contains(5, out int result));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase2Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(3, 3);
                test.Delete(10);
                test.Delete(3);
                Assert.IsFalse(test.Contains(10, out int result));
                Assert.IsFalse(test.Contains(3, out int result2));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase34Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(25, 25);
                test.Put(3, 3);
                test.Delete(10);
                test.Delete(5);
                test.Delete(15);
                test.Delete(3);
                Assert.IsFalse(test.Contains(10, out int result));
                Assert.AreEqual(test.Root.Value, 40);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase5Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(60, 60);
                test.Put(3, 3);
                test.Put(6, 6);
                test.Put(7, 7);
                test.Delete(15);
                Assert.IsFalse(test.Contains(15, out int result));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase6Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(3, 3);
                test.Delete(10);
                test.Delete(5);
                Assert.IsFalse(test.Contains(10, out int result));
                Assert.IsTrue(test.Contains(15, out int result2));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteCase78Test()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(60, 60);
                test.Put(3, 3);
                test.Put(17, 17);
                test.Put(19, 19);
                test.Put(14, 14);
                test.Delete(40);
                test.Delete(30);
                Assert.IsFalse(test.Contains(30, out int result));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        #endregion

        [TestMethod]
        public void ForeachTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(60, 60);
                test.Put(3, 3);
                test.Put(6, 6);
                test.Put(7, 7);
                test.Delete(15);
                foreach(var item in test.InOrder())
                {

                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void ContainsTest()
        {
            try
            {
                RedBlackTree<int, int> test = new RedBlackTree<int, int>();
                test.Put(20, 20);
                test.Put(10, 10);
                test.Put(40, 40);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(30, 30);
                test.Put(50, 50);
                test.Put(60, 60);
                test.Put(3, 3);
                test.Put(6, 6);
                test.Put(7, 7);
                Assert.IsTrue(test.Contains(30, out int result));
                Assert.IsFalse(test.Contains(88, out int result2));
                Assert.AreEqual(test[120], 0);
                Assert.AreEqual(test[6], 6);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
           
        }

        [TestMethod]
        public void ClearTest()
        {
            RedBlackTree<int, int> test = new RedBlackTree<int, int>();
            test.Put(10, 10);
            test.Put(13, 13);
            test.Put(11, 11);
            test.Put(20, 20);
            test.Put(16, 16);
            test.Put(21, 21);
            test.Put(4, 4);
            test.Put(2, 2);
            test.Put(1, 1);
            test.Put(5, 5);
            test.Put(7, 7);
            test.Put(15, 15);
            test.Put(17, 17);
            test.Clear();
            Assert.IsNull(test.Root);
            Assert.AreEqual(0, test.Count);
        }
    }
}
