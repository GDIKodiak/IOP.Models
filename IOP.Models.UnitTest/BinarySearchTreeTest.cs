﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IOP.Models.Tree.BinaryTree;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using IOP.Models.Tree;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class BinarySearchTreeTest
    {
        [TestMethod]
        public void NullTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                Assert.ThrowsException<NullReferenceException>(() => test.Delete(0));
                Assert.ThrowsException<NullReferenceException>(() => test.Get(0));
                Assert.ThrowsException<NullReferenceException>(() => test.FindMax());
                Assert.ThrowsException<NullReferenceException>(() => test.FindMin());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void InsrtTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(0, 0);
                test.Put(3, 3);
                test.Put(1, 1);
                test.Put(2, 2);
                var foreachTree = new List<Node<int, int>>(test.InOrder());
                for (int i = 0; i < foreachTree.Count - 1; i++)
                {
                    if (foreachTree[i].Key > foreachTree[i + 1].Key) Assert.Fail("This is not BinarySearchTree");
                }
                Assert.AreEqual(foreachTree.Count, test.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void DeleteTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(6, 6);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(20, 20);
                test.Put(4, 4);
                test.Put(10, 10);
                test.Put(20, 20);
                test.Put(3, 3);
                test.Put(8, 8);
                test.Put(13, 13);
                test.Put(25, 25);
                test.Put(2, 2);
                test.Put(7, 7);
                test.Put(9, 9);
                test.Delete(15);
                test.Delete(52);
                test.Delete(7);
                test.Delete(2);
                var foreachTree = new List<Node<int, int>>(test.InOrder());
                Assert.AreEqual(foreachTree.Count, test.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void FindMaxTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(10, 10);
                test.Put(13, 13);
                test.Put(11, 11);
                test.Put(20, 20);
                test.Put(16, 16);
                test.Put(21, 21);
                test.Put(4, 4);
                test.Put(2, 2);
                test.Put(1, 1);
                test.Put(5, 5);
                test.Put(3, 3);
                test.Put(7, 7);
                var max = test.FindMax();
                var foreachTree = new List<Node<int, int>>(test.InOrder());
                Assert.AreEqual(max, foreachTree[foreachTree.Count - 1].Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void FindMinTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(10, 10);
                test.Put(13, 13);
                test.Put(11, 11);
                test.Put(20, 20);
                test.Put(16, 16);
                test.Put(21, 21);
                test.Put(4, 4);
                test.Put(2, 2);
                test.Put(1, 1);
                test.Put(5, 5);
                test.Put(3, 3);
                test.Put(7, 7);
                var min = test.FindMin();
                var foreachTree = new List<Node<int, int>>(test.InOrder());
                Assert.AreEqual(min, foreachTree[0].Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SearchTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(10, 10);
                test.Put(13, 13);
                test.Put(11, 11);
                test.Put(20, 20);
                test.Put(16, 16);
                test.Put(21, 21);
                test.Put(4, 4);
                test.Put(2, 2);
                test.Put(1, 1);
                test.Put(5, 5);
                test.Put(7, 7);
                test.Put(15, 15);
                test.Put(17, 17);
                var x = test.Get(52);
                Assert.AreEqual(0, x);
                var y = test.Get(16);
                Assert.IsNotNull(y);
                var z = test.Get(5);
                Assert.IsNotNull(z);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ContainsTest()
        {
            try
            {
                BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
                test.Put(10, 10);
                test.Put(13, 13);
                test.Put(11, 11);
                test.Put(20, 20);
                test.Put(16, 16);
                test.Put(21, 21);
                test.Put(4, 4);
                test.Put(2, 2);
                test.Put(1, 1);
                test.Put(5, 5);
                test.Put(7, 7);
                test.Put(15, 15);
                test.Put(17, 17);
                var a = test.Contains(52, out int result1);
                Assert.IsFalse(a);
                var b = test.Contains(7, out int result2);
                Assert.IsTrue(b);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void FroeachTest()
        {
            BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
            test.Put(10, 10);
            test.Put(13, 13);
            test.Put(11, 11);
            test.Put(20, 20);
            test.Put(16, 16);
            test.Put(21, 21);
            test.Put(4, 4);
            test.Put(2, 2);
            test.Put(1, 1);
            test.Put(5, 5);
            test.Put(7, 7);
            test.Put(15, 15);
            test.Put(17, 17);
            var foreachTree = new List<Node<int, int>>(test.InOrder());
            foreach(var item in test)
            {
                foreachTree.Add(item as BinaryTreeNode<int, int>);
            }
        }

        [TestMethod]
        public void ClearTest()
        {
            BinarySearchTree<int, int> test = new BinarySearchTree<int, int>();
            test.Put(10, 10);
            test.Put(13, 13);
            test.Put(11, 11);
            test.Put(20, 20);
            test.Put(16, 16);
            test.Put(21, 21);
            test.Put(4, 4);
            test.Put(2, 2);
            test.Put(1, 1);
            test.Put(5, 5);
            test.Put(7, 7);
            test.Put(15, 15);
            test.Put(17, 17);
            test.Clear();
            Assert.IsNull(test.Root);
            Assert.AreEqual(0, test.Count);
        }
    }
}
