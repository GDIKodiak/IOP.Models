﻿using IOP.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class SocketMonitorTest
    {
        private AutoResetEvent Test = new AutoResetEvent(false);

        [TestMethod]
        public void MonitorTest()
        {
            try
            {
                SocketMonitor monitor = null;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Task.Run(async () =>
                {
                    monitor = new SocketMonitor(socket, true, 5);
                    monitor.KeepAliveTimeout += Monitor_KeepAliveTimeout;
                    await Task.Delay(4000);
                    monitor.UpdateLastMessageTime(DateTime.Now);
                });
                Test.WaitOne(9000);
                Assert.IsTrue(monitor.EnableKeepAlive);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void EnableKeepAliveCheckAndStartTest()
        {
            try
            {
                SocketMonitor monitor = null;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Task.Run(async () =>
                {
                    monitor = new SocketMonitor(socket);
                    monitor.KeepAliveTimeout += Monitor_KeepAliveTimeout;
                    monitor.EnableKeepAliveCheckAndStart(5);
                    await Task.Delay(4000);
                    monitor.UpdateLastMessageTime(DateTime.Now);
                });
                Test.WaitOne(9000);
                Assert.IsTrue(monitor.EnableKeepAlive);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void DisableKeepAliveCheckTest()
        {
            try
            {
                SocketMonitor monitor = null;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var task = Task.Run(async() =>
                {
                    monitor = new SocketMonitor(socket, true, 10);
                    await Task.Delay(3000);
                    monitor.DisableKeepAliveCheck();
                });
                Task.WaitAll(task);
                Test.WaitOne(3000);
                Assert.IsFalse(monitor.EnableKeepAlive);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void RestartTest()
        {
            try
            {
                SocketMonitor monitor = null;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var task = Task.Run(async () =>
                {
                    monitor = new SocketMonitor(socket, true, 5);
                    monitor.KeepAliveTimeout += Monitor_KeepAliveTimeout;
                    await Task.Delay(2000);
                    monitor.Restart();
                    await Task.Delay(4000);
                    monitor.DisableKeepAliveCheck();
                });
                Test.WaitOne(8000);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        private void Monitor_KeepAliveTimeout(ISocketMonitor obj)
        {
            throw new Exception("Time out");
        }
    }
}
