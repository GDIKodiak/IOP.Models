﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class RepeatTaskTest
    {
        [TestMethod]
        public void RunRepeatTaskTest()
        {
            int i = 0;
            RepeatTask task = new RepeatTask(() => i += 100, 3, 1000);
            task.RunTask();
            while (task.IsRunning) { }
            Assert.AreEqual(i, 300);
        }

        [TestMethod]
        public void InterruptTest()
        {
            int i = 0;
            RepeatTask task = new RepeatTask(() => i += 100, 3, 1000);
            task.RunTask();
            Task.Delay(2000).ContinueWith((t) =>
            {
                task.Interrupt();
            });
            while (task.IsRunning) { }
            Assert.AreEqual(i, 200);
        }
    }
}
