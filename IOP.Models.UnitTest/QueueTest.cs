﻿using IOP.Models.Queue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class QueueTest
    {
        [TestMethod]
        public void DilatationTest()
        {
            try
            {
                LoopQueue<int> test = new LoopQueue<int>(32);
                test.OnQueueIsFull += Test_OnQueueIsFull;
                Assert.IsFalse(test.Dilatation(16));
                for(int i = 0; i <= 32; i++)
                {
                    test.Enqueue(i);
                }
                Assert.IsFalse(test.IsFull);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void EnqueneTest()
        {
            LoopQueue<int> test = new LoopQueue<int>(1024);
            test.OnQueueHasElements += Test_OnQueueHasElement;
            var task1 = Task.Factory.StartNew(() => {
                try
                {
                    for (int i = 0; i < 512; i++)
                    {
                        test.Enqueue(i);
                    }
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            });
            var task2 = Task.Factory.StartNew(() =>
            {
                try
                {
                    for (int i = 512; i < 1024; i++)
                    {
                        test.Enqueue(i);
                    }
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            });
            Task.WaitAll(new Task[] { task1, task2 });
            Assert.IsTrue(test.IsEmpty);
        }

        [TestMethod]
        public void ConcurrentTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            LoopQueue<int> test = new LoopQueue<int>();
            test.OnQueueIsFull += Test_OnQueueIsFull1;
            Random random = new Random();
            stopwatch.Start();
            try
            {
                List<Task> tasks = new List<Task>();
                for(int i =0; i < 10; i++)
                {
                    var task = Task.Run(() =>
                    {
                        for(int c = 0; c< 1000000;c++) test.Enqueue(random.Next(int.MaxValue));
                    });
                    tasks.Add(task);
                }
                for(int i = 0; i< 10; i++)
                {
                    var task = Task.Run(() =>
                    {
                        for (int c = 0; c < 1000000; c++) test.Dequene(out int item);
                    });
                    tasks.Add(task);
                }
                Task.WaitAll(tasks.ToArray());
                stopwatch.Stop();
                Console.WriteLine(stopwatch.ElapsedMilliseconds);
                Assert.AreEqual(true, test.IsEmpty);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }


        [TestMethod]
        public void IndexPriorityQueueTest()
        {
            int[] a = new int[] { 2, -23, 756, 4, 7, 12345, 232, 55, 33, 78, 66, 234, 12, -56, -89, 1285, 996, 770, 99 };
            IndexPriorityQueue<int, int> test = new IndexPriorityQueue<int, int>();
            foreach (var i in a) test.Enqueue(i, i);
            int[] b = new int[a.Length];
            for (int m = 0; m < b.Length; m++)
            {
                if (!test.TryDequeue(out int o)) 
                    throw new Exception("Failed");
                b[m] = o;
            }
        }

        private void Test_OnQueueIsFull1(LoopQueue<int> obj)
        {
            obj.Dilatation(obj.Count * 2);
        }

        private List<int> result = new List<int>();

        private void Test_OnQueueHasElement(LoopQueue<int> obj)
        {
            while (!obj.IsEmpty)
            {
                obj.Dequene(out int item);
                result.Add(item);
            }
        }

        private void Test_OnQueueIsFull(LoopQueue<int> obj)
        {
            obj.Dilatation(64);
        }



    }
}
