﻿using IOP.Models.Tree.BinaryTree;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class SyncRBTreeTest
    {
        [TestMethod]
        public void SRBTreeInsertTest()
        {
            ConcurrentRBTree<int, int> tree = new ConcurrentRBTree<int, int>();
            tree.Put(33, 33);
            Random random = new Random();
            var task1 = Task.Run(() =>
            {
                for(int i = 0; i < 10; i++)
                {
                    var next = random.Next(1000);
                    tree.Put(next, next);
                }
            });
            var task2 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    var next = random.Next(1000);
                    tree.Put(next, next);
                }
            });
            var task3 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    var next = random.Next(1000);
                    tree.Put(next, next);
                }
            });
            var task4 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    var next = random.Next(1000);
                    tree.Put(next, next);
                }
            });
            var task5 = Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    var next = random.Next(1000);
                    tree.Put(next, next);
                }
            });
            Task.WaitAll(task1, task2, task3, task4, task5);
            var c = tree[33];
            var items = new List<Node<int, int>>(tree.InOrder());
            for(int m = 0; m < items.Count() - 1; m++)
            {
                Assert.IsTrue(items[m].Key.CompareTo(items[m + 1].Key) == -1);
            }
        }

        [TestMethod]
        public void ClearTest()
        {
            ConcurrentRBTree<int, int> test = new ConcurrentRBTree<int, int>();
            test.Put(10, 10);
            test.Put(13, 13);
            test.Put(11, 11);
            test.Put(20, 20);
            test.Put(16, 16);
            test.Put(21, 21);
            test.Put(4, 4);
            test.Put(2, 2);
            test.Put(1, 1);
            test.Put(5, 5);
            test.Put(7, 7);
            test.Put(15, 15);
            test.Put(17, 17);
            test.Clear();
            Assert.IsNull(test.Root);
            Assert.AreEqual(0, test.Count);
        }
    }
}
