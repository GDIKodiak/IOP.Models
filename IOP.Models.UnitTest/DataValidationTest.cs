﻿using IOP.Models.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class DataValidationTest
    {
        [TestMethod]
        public void FirstValidation()
        {
            try
            {
                IDataValidation test = new TestClass();
                Assert.IsTrue(test.HasErrors);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void GetErrorsTest()
        {
            try
            {
                IDataValidation test = new TestClass();
                foreach (var item in test.GetErrors("A"))
                {
                    Assert.IsNotNull(item);
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void GetPropertyErrorsTest()
        {
            try
            {
                TestClass test = new TestClass();
                var result = test.GetPropertyErrors(() => test.A);
                Assert.IsNotNull(result.FirstOrDefault());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void GetPropertyErrorsTest2()
        {
            try
            {
                IDataValidation test = new TestClass();
                Assert.IsTrue(test.GetPropertyErrors("B").Any());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PropertyHasErrorTest()
        {
            try
            {
                TestClass test = new TestClass();
                test.B = "ACC";
                Assert.IsFalse(test.PropertyHasError("B"));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ValidateInstanceTest()
        {
            try
            {
                TestClass test = new TestClass();
                test.B = "ACC";
                test.A = "AAA";
                test.C = "ASADWAAWDAWGAWDAG";
                test.PhoneNumber = "13987257606";
                test.ValidateInstance();
                Assert.IsFalse(test.HasErrors);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SetErrorsTest()
        {
            try
            {
                TestClass test = new TestClass();
                test.B = "ACC";
                test.SetErrors(() => test.B, new string[] { "NewError" });
                Assert.IsTrue(test.PropertyHasError("B"));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SetErrorTest()
        {
            try
            {
                TestClass test = new TestClass();
                test.B = "ACCCCD";
                test.SetError(() => test.B, "NewError");
                Assert.IsTrue(test.PropertyHasError("B"));
                test.RemoveError("B");
                Assert.IsFalse(test.PropertyHasError("B"));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void RemoveError()
        {
            try
            {
                TestClass test = new TestClass();
                test.RemoveError(() => test.B);
                Assert.IsFalse(test.PropertyHasError("B"));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ClearErrorTest()
        {
            try
            {
                IDataValidation test = new TestClass();
                test.ClearErrors();
                Assert.IsFalse(test.HasErrors);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void InitTest()
        {
            TestClassB test = new TestClassB();
            Assert.IsFalse(test.HasErrors);
            test.B = "AWDFWDAWF";
            Assert.IsTrue(test.HasErrors);
            test.B = "A";
            Assert.IsFalse(test.HasErrors);
        }
    }

    class TestClass: DataValidation
    {
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                if (_Id != value)
                {
                    _Id = value;
                    OnPropertyChanged(() => this.Id);
                }
            }
        }

        private string _A;
        [Required(ErrorMessage = "不能为空")]
        public string A
        {
            get { return _A; }
            set
            {
                if (_A != value)
                {
                    _A = value;
                    OnPropertyChanged(() => this.A);
                    ValidateProperty(value, () => this.A);
                }
            }
        }

        private string _B = "AWDAAWDWAGAWDAGAWF";
        [MaxLength(4,ErrorMessage = "不能超过4字符")]
        public string B
        {
            get { return _B; }
            set
            {
                if (_B != value)
                {
                    _B = value;
                    OnPropertyChanged(() => this.B);
                    ValidateProperty(value, () => this.B);
                }
            }
        }

        private string _C = "AC";
        [MinLength(8,ErrorMessage ="至少8个字符")]
        public string C
        {
            get { return _C; }
            set
            {
                if (_C != value)
                {
                    _C = value;
                    OnPropertyChanged(() => this.C);
                    ValidateProperty(value, () => this.C);
                }
            }
        }

        private string _PhoneNumber = "12306";
        [RegularExpression("^[1][3,4,5,7,8][0-9]{9}$",ErrorMessage = "手机号错误")]
        public string PhoneNumber
        {
            get { return _PhoneNumber; }
            set
            {
                if (_PhoneNumber != value)
                {
                    _PhoneNumber = value;
                    OnPropertyChanged(() => this.PhoneNumber);
                    ValidateProperty(value, () => this.PhoneNumber);
                }
            }
        }


    }

    class TestClassB : DataValidation
    {
        private string _A = "BB";
        [Required(ErrorMessage = "不能为空")]
        public string A
        {
            get { return _A; }
            set
            {
                if (_A != value)
                {
                    _A = value;
                    OnPropertyChanged(() => this.A);
                    ValidateProperty(value, () => this.A);
                }
            }
        }

        private string _B = "AW";
        [MaxLength(4, ErrorMessage = "不能超过4字符")]
        public string B
        {
            get { return _B; }
            set
            {
                if (_B != value)
                {
                    _B = value;
                    OnPropertyChanged(() => this.B);
                    ValidateProperty(value, () => this.B);
                }
            }
        }

        private string _C = "ACCCSDAWDADWAFAWDF";
        [MinLength(8, ErrorMessage = "至少8个字符")]
        public string C
        {
            get { return _C; }
            set
            {
                if (_C != value)
                {
                    _C = value;
                    OnPropertyChanged(() => this.C);
                    ValidateProperty(value, () => this.C);
                }
            }
        }
    }
}
