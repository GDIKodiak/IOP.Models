﻿using IOP.Models.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class DataMonitorTest
    {
        [TestMethod]
        public void ChangedPropertyTest()
        {
            MonitorTest monitor = new MonitorTest();
            Test2 test = new Test2("A", 25);
            monitor.MyTest = test;
            Test2 test2 = new Test2("B", 36);
            DataMonitor<MonitorTest> dataMonitor = new DataMonitor<MonitorTest>(monitor, DataState.Create);
            monitor.TestA = 125;
            monitor.TestD = "AAACCDD";
            monitor.TestD = null;
            Assert.IsTrue(dataMonitor.IsChanged);
            dataMonitor.Retroversion();
            dataMonitor.Retroversion();
            dataMonitor.Retroversion();
            Assert.IsFalse(dataMonitor.IsChanged);
            monitor.MyTest = test2;
            dataMonitor.Retroversion();
            Assert.IsFalse(dataMonitor.IsChanged);
        }
    }

    class MonitorTest : DataValidation
    {
        private int _TestA;
        [Range(1,126, ErrorMessage ="错误范围")]
        public int TestA
        {
            get { return _TestA; }
            set
            {
                if (_TestA != value)
                {
                    OnDataStampChanged("TestA", _TestA, value);
                    _TestA = value;
                    OnPropertyChanged(() => TestA);
                    ValidateProperty(value, () => TestA);
                }
            }
        }

        private long _TestB;
        [Range(1, 288, ErrorMessage = "错误范围")]
        public long TestB
        {
            get { return _TestB; }
            set
            {
                if (_TestB != value)
                {
                    OnDataStampChanged("TestB", _TestB, value);
                    _TestB = value;
                    OnPropertyChanged(() => TestB);
                    ValidateProperty(value, () => TestB);
                }
            }
        }


        private string _TestC;
        [Required(ErrorMessage = "不能为空")]
        public string TestC
        {
            get { return _TestC; }
            set
            {
                if (_TestC != value)
                {
                    OnDataStampChanged("TestC", _TestC, value);
                    _TestC = value;
                    OnPropertyChanged(() => TestC);
                    ValidateProperty(value, () => TestC);
                }
            }
        }

        private string _TestD;
        [Required(ErrorMessage = "不能为空")]
        public string TestD
        {
            get { return _TestD; }
            set
            {
                if (_TestD != value)
                {
                    OnDataStampChanged("TestD", _TestD, value);
                    _TestD = value;
                    OnPropertyChanged(() => TestD);
                    ValidateProperty(value, () => TestD);
                }
            }
        }

        private Test2 _MyTest;
        public Test2 MyTest
        {
            get { return _MyTest; }
            set
            {
                if (_MyTest != value)
                {
                    OnDataStampChanged("MyTest", _MyTest, value);
                    _MyTest = value;
                    OnPropertyChanged(() => MyTest);
                }
            }
        }

    }

    class Test2
    {
        public string A { get; set; }

        public int B { get; set; }

        public Test2(string a, int b)
        {
            A = a;
            B = b;
        }
    }
}
