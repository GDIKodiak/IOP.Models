﻿using IOP.Models.Tree.BinaryTree;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOP.Models.UnitTest
{
    [TestClass]
    public class AVLTreeTest
    {
        [TestMethod]
        public void RRInsertTest()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(5, 5);
                test.Put(6, 6);
                test.Put(7, 7);
                test.Put(8, 8);
                test.Put(9, 9);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void LLInsertTest()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(5, 5);
                test.Put(4, 4);
                test.Put(3, 3);
                test.Put(2, 2);
                test.Put(1, 1);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void LRInsertTest()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(12, 12);
                test.Put(8, 8);
                test.Put(4, 4);
                test.Put(2, 2);
                test.Put(6, 6);
                test.Put(5, 5);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void RLInsertTest()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(4, 4);
                test.Put(8, 8);
                test.Put(12, 12);
                test.Put(14, 14);
                test.Put(10, 10);
                test.Put(9, 9);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void RRDelelte()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(10, 10);
                test.Put(15, 15);
                test.Put(5, 5);
                test.Put(11, 11);
                test.Put(16, 16);
                test.Delete(5);
                Assert.AreEqual(15, test.Root.Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void LLDelete()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(10, 10);
                test.Put(13, 13);
                test.Put(8, 8);
                test.Put(7, 7);
                test.Put(9, 9);
                test.Delete(13);
                Assert.AreEqual(8, test[8]);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void LRDelelte()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(10, 10);
                test.Put(5, 5);
                test.Put(13, 13);
                test.Put(11, 11);
                test.Put(4, 4);
                test.Put(9, 9);
                test.Put(7, 7);
                test.Delete(11);
                Assert.AreEqual(9, test.Root.Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void RLDelete()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(8, 8);
                test.Put(4, 4);
                test.Put(12, 12);
                test.Put(2, 2);
                test.Put(10, 10);
                test.Put(14, 14);
                test.Put(9, 9);
                test.Delete(2);
                Assert.AreEqual(10, test.Root.Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        /// <summary>
        /// 要删除的节点的子节点都不为Null时删除测试
        /// </summary>
        [TestMethod]
        public void ChildrenAllNotNullDelete()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(8, 8);
                test.Put(4, 4);
                test.Put(10, 10);
                test.Put(9, 9);
                test.Put(12, 12);
                test.Put(2, 2);
                test.Put(11, 11);
                test.Delete(10);
                Assert.AreEqual(8, test.Root.Value);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ChildrenAllNotNullDelete2()
        {
            try
            {
                AVLTree<int, int> test = new AVLTree<int, int>();
                test.Put(6, 6);
                test.Put(5, 5);
                test.Put(15, 15);
                test.Put(4, 4);
                test.Put(10, 10);
                test.Put(20, 20);
                test.Put(3, 3);
                test.Put(8, 8);
                test.Put(13, 13);
                test.Put(25, 25);
                test.Put(2, 2);
                test.Put(7, 7);
                test.Put(9, 9);
                test.Delete(15);
                test.Delete(13);
                test.Delete(7);
                test.Delete(2);
                Assert.AreEqual(6, test.Root.Value);
                Assert.AreEqual(4, test.Root.Height);
                var foreachTree = test.InOrder();
                Assert.AreEqual(foreachTree.Count(), test.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void ClearTest()
        {
            AVLTree<int, int> test = new AVLTree<int, int>();
            test.Put(10, 10);
            test.Put(13, 13);
            test.Put(11, 11);
            test.Put(20, 20);
            test.Put(16, 16);
            test.Put(21, 21);
            test.Put(4, 4);
            test.Put(2, 2);
            test.Put(1, 1);
            test.Put(5, 5);
            test.Put(7, 7);
            test.Put(15, 15);
            test.Put(17, 17);
            test.Clear();
            Assert.IsNull(test.Root);
            Assert.AreEqual(0, test.Count);
        }
    }
}
