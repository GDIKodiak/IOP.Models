﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Query
{
    /// <summary>
    /// 条件参数模型
    /// </summary>
    public class WhereParameter
    {
        /// <summary>
        /// 参数名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 参数值
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// 操作符
        /// </summary>
        public Operator Operator { get; set; } = Operator.Equal;

        /// <summary>
        /// 连接符
        /// </summary>
        public Connector Connector { get; set; } = Connector.AND;
    }
}
