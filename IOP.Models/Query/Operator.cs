﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Query
{
    /// <summary>
    /// 操作符
    /// </summary>
    public enum Operator
    {
        /// <summary>
        /// 恒等于
        /// </summary>
        Equal,
        /// <summary>
        /// 不等于
        /// </summary>
        NotEqual,
        /// <summary>
        /// 大于等于
        /// </summary>
        GreaterThanOrEqual,
        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan,
        /// <summary>
        /// 小于
        /// </summary>
        LessThan,
        /// <summary>
        /// 小于等于
        /// </summary>
        LessThanOrEqual,
        /// <summary>
        /// 包含
        /// </summary>
        IN
    }
}
