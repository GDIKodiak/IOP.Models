﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Query
{
    /// <summary>
    /// 连接符
    /// </summary>
    public enum Connector
    {
        /// <summary>
        /// 与
        /// </summary>
        AND,
        /// <summary>
        /// 或
        /// </summary>
        OR
    }
}
