﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Tree.BinaryTree
{
    /// <summary>
    /// 红黑树节点
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class RedBlackTreeNode<TKey, TValue> : Node<TKey, TValue>
        where TKey : IComparable<TKey>
    {

        /// <summary>
        /// 节点颜色
        /// </summary>
        public bool Color { get; set; } = false;

        /// <summary>
        /// 父节点
        /// </summary>
        public RedBlackTreeNode<TKey, TValue> Parent { get; set; } = null;

        /// <summary>
        /// 左子节点
        /// </summary>
        public RedBlackTreeNode<TKey, TValue> Left { get; set; } = null;

        /// <summary>
        /// 右子节点
        /// </summary>
        public RedBlackTreeNode<TKey, TValue> Right { get; set; } = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="color">颜色</param>
        public RedBlackTreeNode(TKey key, TValue value, bool color = true)
        {
            Key = key;
            Value = value;
            Color = color;
        }
    }
}
