﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Tree.BinaryTree
{
    /// <summary>
    /// 二叉树节点
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class BinaryTreeNode<TKey, TValue> : Node<TKey, TValue>
        where TKey : IComparable<TKey>
    {
        /// <summary>
        /// 父节点
        /// </summary>
        public BinaryTreeNode<TKey, TValue> Parent { get; set; } = null;

        /// <summary>
        /// 左子节点
        /// </summary>
        public BinaryTreeNode<TKey, TValue> Left { get; set; } = null;

        /// <summary>
        /// 右子节点
        /// </summary>
        public BinaryTreeNode<TKey, TValue> Right { get; set; } = null;

        /// <summary>
        /// 相对高度
        /// </summary>
        public int Height { get; set; } = 0;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public BinaryTreeNode(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}
