﻿using System;
using System.Collections.Generic;

namespace IOP.Models.Tree.BinaryTree
{
    /// <summary>
    /// 并发红黑树
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class ConcurrentRBTree<TKey, TValue> : RedBlackTree<TKey, TValue>
        where TKey : IComparable<TKey>
    {
        /// <summary>
        /// 线程锁
        /// </summary>
        private readonly object SyncRoot = new object();

        /// <summary>
        /// 索引
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override TValue this[TKey key] => Get(key);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override TValue Get(TKey key)
        {
            lock (SyncRoot)
            {
                return base.Get(key);
            }
        }

        /// <summary>
        /// 是否包括
        /// </summary>
        /// <param name="key"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool Contains(TKey key, out TValue result)
        {
            if (Root == null) throw new NullReferenceException("No data in this tree");
            lock (SyncRoot)
            {
                return base.Contains(key, out result);
            }
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public override void Put(TKey key, TValue value)
        {
            lock(SyncRoot)
            {
                base.Put(key, value);
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key"></param>
        public override void Delete(TKey key)
        {
            lock (SyncRoot)
            {
                base.Delete(key);
            }
        }
        /// <summary>
        /// 清空
        /// </summary>
        public override void Clear()
        {
            lock (SyncRoot)
            {
                base.Clear();
            }
        }

        /// <summary>
        /// 最大值
        /// </summary>
        /// <returns></returns>
        public override TValue FindMax()
        {
            lock (SyncRoot)
            {
                return base.FindMax();
            }
        }
        /// <summary>
        /// 最小值
        /// </summary>
        /// <returns></returns>
        public override TValue FindMin()
        {
            lock (SyncRoot)
            {
                return base.FindMin();
            }
        }

        /// <summary>
        /// 前序遍历
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<Node<TKey, TValue>> PreOrder()
        {
            lock (SyncRoot)
            {
                return base.PreOrder();
            }
        }
        /// <summary>
        /// 中序遍历
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<Node<TKey, TValue>> InOrder()
        {
            lock (SyncRoot)
            {
                return base.InOrder();
            }
        }
        /// <summary>
        /// 后序遍历
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<Node<TKey, TValue>> PostOrder()
        {
            lock (SyncRoot)
            {
                return base.PostOrder();
            }
        }
    }
}
