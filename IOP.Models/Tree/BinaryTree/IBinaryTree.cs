﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Tree.BinaryTree
{
    /// <summary>
    /// 二叉树接口
    /// </summary>
    public interface IBinaryTree<TKey, TValue> : IEnumerable<Node<TKey,TValue>>
        where TKey : IComparable<TKey>
    {

        /// <summary>
        /// 树节点总数
        /// </summary>
        int Count { get; set; }

        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Put(TKey key, TValue value);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key"></param>
        void Delete(TKey key);

        /// <summary>
        /// 是否包含某个节点
        /// </summary>
        /// <param name="key"></param>
        /// <param name="result">返回结果</param>
        /// <returns></returns>
        bool Contains(TKey key, out TValue result);

        /// <summary>
        /// 清空
        /// </summary>
        void Clear();

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        TValue Get(TKey key);

        /// <summary>
        /// 查找最大值
        /// </summary>
        /// <returns></returns>
        TValue FindMax();

        /// <summary>
        /// 查找最小值
        /// </summary>
        /// <returns></returns>
        TValue FindMin();

        /// <summary>
        /// 索引
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        TValue this[TKey key] { get; }

        /// <summary>
        /// 先序遍历
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node<TKey, TValue>> PreOrder();

        /// <summary>
        /// 中序遍历
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node<TKey, TValue>> InOrder();

        /// <summary>
        /// 后序遍历
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node<TKey, TValue>> PostOrder();
    }
}
