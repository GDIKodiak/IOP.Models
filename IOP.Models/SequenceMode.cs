﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models
{
    /// <summary>
    /// 序列模式
    /// </summary>
    public enum SequenceMode
    {
        /// <summary>
        /// 最大值
        /// </summary>
        MAX,
        /// <summary>
        /// 最小值
        /// </summary>
        MIN
    }
}
