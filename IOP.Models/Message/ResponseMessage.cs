﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Message
{
    /// <summary>
    /// 通用请求回执消息模型
    /// </summary>
    public class ResponseMessage<T>
    {
        /// <summary>
        /// 返回代号
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 请求状态
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// 请求消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回的数据
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public ResponseMessage() { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="code"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        public ResponseMessage(int code, bool status, string message = "", T data = default)
        {
            Code = code;
            Status = status;
            Message = message;
            Data = data;
        }
    }
}
