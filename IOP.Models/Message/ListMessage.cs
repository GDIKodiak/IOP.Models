﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Message
{
    /// <summary>
    /// 列表消息
    /// </summary>
    /// <typeparam name="Titem">列表项</typeparam>
    public class ListMessage<Titem>
    {
        /// <summary>
        /// 列表
        /// </summary>
        public IEnumerable<Titem> List { get; set; }
        /// <summary>
        /// 大小
        /// </summary>
        public int Count { get; set; } = 0;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="lists"></param>
        /// <param name="count"></param>
        public ListMessage(IEnumerable<Titem> lists, int count)
        {
            List = lists;
            Count = count;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        public ListMessage() { }
    }
}
