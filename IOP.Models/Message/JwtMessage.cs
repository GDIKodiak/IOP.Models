﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models.Message
{
    /// <summary>
    /// jwt消息模型
    /// </summary>
    public class JwtMessage
    {
        /// <summary>
        /// 认证令牌
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public long ExpiresIn { get; set; }

        /// <summary>
        /// 令牌类型
        /// </summary>
        public string TokenType { get; set; }
    }
}
