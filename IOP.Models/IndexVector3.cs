﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models
{
    /// <summary>
    /// 下标向量
    /// </summary>
    public struct IndexVector3
    {
        /// <summary>
        /// X
        /// </summary>
        public int X;
        /// <summary>
        /// Y
        /// </summary>
        public int Y;
        /// <summary>
        /// Z
        /// </summary>
        public int Z;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public IndexVector3(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
