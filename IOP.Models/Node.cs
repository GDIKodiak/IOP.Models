﻿using System;

namespace IOP.Models
{
    /// <summary>
    /// 节点基类
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class Node<TKey, TValue>
        where TKey: IComparable<TKey>
    {
        /// <summary>
        /// 键
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public Node() { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public Node(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}
