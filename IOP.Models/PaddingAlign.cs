﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Models
{
    /// <summary>
    /// 布局对其类
    /// </summary>
    public static class PaddingAlign
    {
#if ARM64
        /// <summary>
        /// ARM64 CPU缓存行长度
        /// </summary>
        public const int CPU_CACHE_LINE = 128;
#else
        /// <summary>
        /// Intel CPU缓存行长度
        /// </summary>
        public const int CPU_CACHE_LINE = 64;
#endif

        /// <summary>
        /// 输出一个数，使之大于或等于输入的参数并且满足2^n
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int RoundUpToPowerOf2(this int number)
        {
            // Based on https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
            --number;
            number |= number >> 1;
            number |= number >> 2;
            number |= number >> 4;
            number |= number >> 8;
            number |= number >> 16;
            return number + 1;
        }
    }
}
