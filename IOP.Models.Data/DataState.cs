﻿namespace IOP.Models.Data
{
    /// <summary>
    /// 模型状态
    /// </summary>
    public enum DataState
    {
        /// <summary>
        /// 创建
        /// </summary>
        Create,
        /// <summary>
        /// 更新
        /// </summary>
        Update
    }
}
