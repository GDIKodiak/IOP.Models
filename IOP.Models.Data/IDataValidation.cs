﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace IOP.Models.Data
{
    /// <summary>
    /// 可验证的数据接口
    /// </summary>
    public interface IDataValidation : INotifyPropertyChanged, INotifyPropertyChanging, INotifyDataErrorInfo, IDisposable
    {
        /// <summary>
        /// 数据戳发生变更事件
        /// </summary>
        event Action<DataStamp> DataStampChanged;
        /// <summary>
        /// 获取指定属性的验证错误信息
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        IEnumerable<string> GetPropertyErrors<TProperty>(Expression<Func<TProperty>> expression);
        /// <summary>
        /// 获取指定属性的验证错误信息
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        IEnumerable<string> GetPropertyErrors(string propertyName);
        /// <summary>
        /// 判定属性是否有错误
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        bool PropertyHasError(string propertyName);
        /// <summary>
        /// 设置属性多条错误信息
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="propertyErrors"></param>
        void SetErrors(string propertyName, IEnumerable<string> propertyErrors);
        /// <summary>
        /// 设置属性多条错误信息
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="propertyErrors"></param>
        void SetErrors<TProperty>(Expression<Func<TProperty>> expression, IEnumerable<string> propertyErrors);
        /// <summary>
        /// 设置属性错误信息
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errorMassage"></param>
        void SetError(string propertyName, string errorMassage);
        /// <summary>
        /// 设置属性错误信息
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="errorMessage"></param>
        void SetError<TProperty>(Expression<Func<TProperty>> expression, string errorMessage);
        /// <summary>
        /// 移除错误信息
        /// </summary>
        /// <param name="propertyName"></param>
        void RemoveError(string propertyName);
        /// <summary>
        /// 移除错误信息
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression"></param>
        void RemoveError<TProperty>(Expression<Func<TProperty>> expression);
        /// <summary>
        /// 清除所有错误
        /// </summary>
        void ClearErrors();
        /// <summary>
        /// 错误列表
        /// </summary>
        IEnumerable<string> ErrorsList { get; }
        /// <summary>
        /// 验证实例
        /// </summary>
        void ValidateInstance();
        /// <summary>
        /// 验证单条属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="value"></param>
        /// <param name="expression"></param>
        void ValidateProperty<T, TProperty>(T value, Expression<Func<TProperty>> expression);
        /// <summary>
        /// 验证单条属性
        /// </summary>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        void ValidateProperty(object value, string propertyName);
    }
}
