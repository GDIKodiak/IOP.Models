﻿using System;

namespace IOP.Models.Data.Attributes
{
    /// <summary>
    /// 忽略属性标签
    /// </summary>
    public class IgnorePropertyAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public IgnorePropertyAttribute() { }
    }
}
