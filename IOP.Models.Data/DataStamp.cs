﻿namespace IOP.Models.Data
{
    /// <summary>
    /// 数据戳
    /// </summary>
    public class DataStamp
    {
        /// <summary>
        /// 属性名
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// 起始值
        /// </summary>
        public object StartValue { get; set; }
        /// <summary>
        /// 结束值
        /// </summary>
        public object EndValue { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="startValue"></param>
        /// <param name="endValue"></param>
        public DataStamp(string propertyName, object startValue, object endValue)
        {
            PropertyName = propertyName;
            StartValue = startValue;
            EndValue = endValue;
        }
    }
}
