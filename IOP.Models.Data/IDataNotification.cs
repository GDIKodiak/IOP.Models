﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace IOP.Models.Data
{
    /// <summary>
    /// 可通知的数据接口
    /// </summary>
    public interface IDataNotification : INotifyPropertyChanged, INotifyPropertyChanging
    {
        /// <summary>
        /// 当数据发生变更时
        /// </summary>
        /// <param name="propertyName"></param>
        void OnPropertyChanged(string propertyName);
        /// <summary>
        /// 当数据发生变更时
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression"></param>
        void OnPropertyChanged<TProperty>(Expression<Func<TProperty>> expression);
        /// <summary>
        /// 当数据发生变更时
        /// </summary>
        /// <param name="propertyName"></param>
        void OnPropertyChanging(string propertyName);
        /// <summary>
        /// 当属性发生变更时
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression"></param>
        void OnPropertyChanging<TProperty>(Expression<Func<TProperty>> expression);
    }
}
